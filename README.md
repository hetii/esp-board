# ESP-BOARD #

This is simple experimental board based on esp8266-esp-03

### What is this repository for? ###

* Version 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Follow [esphome](https://esphome.io/index.html) project.

### TODOs ###

* Update code to kicad 7.0

### Prototype board: ###

![espboard_circuit.jpg](https://bitbucket.org/hetii/esp-board/raw/bccf7197b923f27887fba1ff672780ff60ab3ec2/images/circuit.png)
![espboard_board.jpg](https://bitbucket.org/hetii/esp-board/raw/bccf7197b923f27887fba1ff672780ff60ab3ec2/images/board.png)

